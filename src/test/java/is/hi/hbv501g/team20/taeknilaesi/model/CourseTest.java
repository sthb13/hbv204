package is.hi.hbv501g.team20.taeknilaesi.model;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

public class CourseTest {
    private Course c1;
    private Course c2;

    @BeforeEach
    void construcCourseTestObjects(){
        c1 = new Course(1, "Course 1", "Introduction", new ArrayList<>());
        c2 = new Course(2, "Course 2", "Chapter 2", new ArrayList<>());
    }

    @Test
    public void testEquals(){
        Course equalCourse = new Course(1, "Course 1", "Introduction", new ArrayList<>());
        assertTrue(c1.equals(c1));
        assertFalse(c1.equals(equalCourse));
        assertTrue(c1.getTitle().equals("Course 1"));
        assertTrue(c2.getDescription().equals("Chapter 2"));
    }
}
