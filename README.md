[![pipeline status](https://gitlab.com/sthb13/hbv204/badges/main/pipeline.svg)](https://gitlab.com/sthb13/hbv204/-/commits/main)
[![coverage report](https://gitlab.com/sthb13/hbv204/badges/main/coverage.svg)](https://gitlab.com/sthb13/hbv204/-/commits/main)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=sthb13_hbv204&metric=bugs)](https://sonarcloud.io/summary/new_code?id=sthb13_hbv204)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=sthb13_hbv204&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=sthb13_hbv204)

# HBV501G Hugbúnaðarverkefni 1
Software Development project at the University of Iceland. This is a web application that hosts videos in icelandic, aimed at elderly citizens, that teaches how to use an Android phone and tablet.

## Requirements
### Java 14
```java --version~```
### Maven 3.6.3
(may run on earlier versions)

```mvn -v```
## Install, run and test
To run the project extract the zip file or alternatively

```git clone https://gitlab.com/sthb13/hbv204.git```

then enter the root directory of the project

```cd hbv204```

and run the application by executing the following Maven command

```mvn clean spring-boot:run```

Open the web application in your browser at ```http://localhost:8080/```

To run tests use the following command

```mvn test```

or to run individual unit tests enter

```mvn test -Dtest=CommentTest```

Individual tests can also be separated by a comma such as

```mvn test -Dtest=CommentTest,CourseTest```

Coverage reports are available under `target/site/jacoco/index.html` which can be opened with a browser. Does not work currently on GitLab but works on local machine.

## About 
The application is built using [Spring Boot](https://spring.io/projects/spring-boot), an opinionated 'batteries included' starter template to build applications using the [Spring](https://spring.io/) framework. It consists of [Spring Data Rest](https://spring.io/projects/spring-data-rest), [Spring Security](https://spring.io/projects/spring-security), [Spring Session Core](https://spring.io/projects/spring-session-core), [Spring Data JPA](https://spring.io/projects/spring-data-jpa) and some web related tools like Starter Web, DevTools and Starter Mail.
Furthermore it uses the [Thymeleaf](https://www.thymeleaf.org/) templating engine to generate and render the web requests.
The default configuration runs with an in-memory setting of [H2](https://www.h2database.com/html/main.html) but can be changed to use an online [mySQL](https://www.mysql.com/) service as well, although it is currently not active.
The videos are hosted at servers provided by [Háskóli Íslands](https://www.hi.is/).
